//stateless functional component(JS functions) vs stateful class component(should contain render method)
import React from "react";
import { Provider } from "react-redux";
import Cart from "./components/Cart";
import Filter from "./components/Filter";
import Products from "./components/Products";
// import data from "./data.json";
import store from "./store.js";

class App extends React.Component {
  // constructor() {
  //   super();
  //   this.state = {
  // products: data.products /*for products */,
  /*for filtering */
  // size: "",
  // sort: "",
  /* cart */
  //cartItems: []
  //     cartItems: localStorage.getItem("cartItems")
  //       ? JSON.parse(localStorage.getItem("cartItems"))
  //       : [],
  //   };
  // }

  // addToCart = (product) => {
  //   const cartItems = this.state.cartItems.slice(); //clone copy
  //   let alreadyInCart = false;
  //   cartItems.forEach((item) => {
  //     if (item._id === product._id) {
  //       item.count++;
  //       alreadyInCart = true;
  //     }
  //   });
  //   if (!alreadyInCart) {
  //     cartItems.push({ ...product, count: 1 }); //add an item in cart with cout property set up to 1(single piece)
  //   }
  //   this.setState({ cartItems });
  // };
  // createOrder = (order) => {
  //   alert("Need to save later" + order.name);
  // };
  // removeFromCart = (product) => {
  //   const cartItems = this.state.cartItems.slice(); //clone copy
  //   //set current state to remove the product selected to be removed
  //   this.setState({
  //     cartItems: cartItems.filter((item) => item._id !== product._id),
  //   });
  // };

  // //parameters for bellow functions are received from Filter component state
  // //sort products function
  // sortProducts = (event) => {
  //   //console.log(event.target.value);
  //   const sortValue = event.target.value;
  //   this.setState((state) => ({
  //     sort: sortValue,
  //     products: state.products
  //       .slice()
  //       .sort((a, b) =>
  //         sortValue === "lowest"
  //           ? a.price >= b.price
  //             ? 1
  //             : -1
  //           : sortValue === "highest"
  //           ? a.price <= b.price
  //             ? 1
  //             : -1
  //           : a.year <= b.year
  //           ? 1
  //           : -1
  //       ),
  //   }));
  // };

  // //show a specified number of products
  // showProducts = (event) => {
  //   //console.log(event.target.value);
  //   this.setState({
  //     size: event.target.value,
  //     products: data.products.slice(event.target.value),
  //   });
  // };

  render() {
    return (
      <Provider store={store}>
        <div className="grid-container">
          <header>
            <a href="/">Wonderland Books</a>
          </header>
          <main>
            <div className="content">
              <div className="main">
                <Filter />
                <Products />
              </div>
              <div className="sidebar">
                <Cart />
              </div>
            </div>
          </main>
          <footer>All rights reserved</footer>
        </div>
      </Provider>
    );
  }
}

export default App;
