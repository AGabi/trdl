import React, { Component } from "react";
import formatCurrency from "../util";
import Fade from "react-reveal/Fade";
import Modal from "react-modal";
import Zoom from "react-reveal/Zoom";
import { connect } from "react-redux";
import { removeFromCart } from "../actions/cartActions";
import { createOrder, clearOrder } from "../actions/orderActions";

class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      address: "",
      showCheckout: false,
    };
  }

  handleInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  createOrder = (e) => {
    e.preventDefault();
    console.log("idiot");
    const order = {
      name: this.state.name,
      email: this.state.email,
      address: this.state.address,
      createdAt: Date.now(),
      cartItems: this.props.cartItems,
      total: this.props.cartItems.reduce((a, c) => a + c.price * c.count, 0),
    };
    this.props.createOrder(order); //send this to parent to be saved as order for user
  };
  closeModal = () => {
    this.props.clearOrder();
  };
  render() {
    const { cartItems, order } = this.props;
    let a = "item";
    return (
      <div>
        {cartItems.length === 0 ? (
          <div className="cart cart-header">Cart is empty</div>
        ) : (
          <div className="cart cart-header">
            You have {cartItems.length} {cartItems.length > 1 ? (a += "s") : a}{" "}
            in the cart {""}
          </div>
        )}
        {order && (
          <Modal
            isOpen={true}
            onRequestClose={this.closeModal}
            // className="order-modal"
          >
            <Zoom>
              <button className="close-modal" onClick={this.closeModal}>
                x
              </button>
              <div className="order-details">
                <h3 className="success-message">Your order has been placed!</h3>
                <h2>Order {order._id}</h2>
                <ul>
                  <li>
                    <div>Name:</div>
                    <div>{order.name}</div>
                  </li>
                  <li>
                    <div>Email:</div>
                    <div>{order.email}</div>
                  </li>
                  <li>
                    <div>Address:</div>
                    <div>{order.address}</div>
                  </li>
                  <li>
                    <div>Date:</div>
                    <div>{order.createdAt}</div>
                  </li>
                  <li>
                    <div>Cart items:</div>
                    <div>
                      {order.cartItems.map((x) => (
                        <div>
                          {x.count} {" x "} {x.title}
                        </div>
                      ))}
                    </div>
                  </li>
                  <li>
                    <div>Total:</div>
                    <div>{formatCurrency(order.total)}</div>
                  </li>
                </ul>
              </div>
            </Zoom>
          </Modal>
        )}
        <div>
          <div className="cart">
            <Fade left cascade>
              <ul className="cart-items">
                {cartItems.map((item) => (
                  <li key={item._id}>
                    <div>
                      <img src={item.image} alt={item.title} />
                    </div>
                    <div>
                      <div>{item.title}</div>
                      <div className="right">
                        {formatCurrency(item.price)} x {item.count}{" "}
                        <button
                          className="button"
                          onClick={() => this.props.removeFromCart(item)}
                        >
                          Remove
                        </button>
                      </div>
                    </div>
                  </li>
                ))}
              </ul>
            </Fade>
          </div>

          {/* Hide if there were no articles in cart */}
          {cartItems.length !== 0 && (
            <div>
              <div className="cart">
                <div className="total">
                  <div>
                    Total:{" "}
                    {formatCurrency(
                      cartItems.reduce(
                        (accumulator, currentItem) =>
                          accumulator + currentItem.price * currentItem.count,
                        0
                      )
                    )}
                  </div>

                  <button
                    className="button primary"
                    onClick={() => {
                      this.setState({ showCheckout: true });
                    }}
                  >
                    Proceed Order
                  </button>
                </div>
              </div>
              {this.state.showCheckout === true && (
                <div className="cart">
                  <form onSubmit={this.createOrder}>
                    <Fade right cascade>
                      <ul className="form-container">
                        <li>
                          <label>Email</label>
                          <input
                            name="email"
                            type="email"
                            required
                            onChange={this.handleInput}
                          ></input>
                        </li>
                        <li>
                          <label>Name</label>
                          <input
                            name="name"
                            type="text"
                            required
                            onChange={this.handleInput}
                          ></input>
                        </li>
                        <li>
                          <label>Address</label>
                          <input
                            name="address"
                            type="text"
                            required
                            onChange={this.handleInput}
                          ></input>
                        </li>
                        <li>
                          <button type="submit" className="button primary">
                            Checkout
                          </button>
                        </li>
                      </ul>
                    </Fade>
                  </form>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    order: state.order.order,
    cartItems: state.cart.cartItems,
  }),
  {
    removeFromCart,
    createOrder,
    clearOrder,
  }
)(Cart);

//border-bottom is not applied on a functional component. TODO.. Check why!!
// function Cart(props) {
//     const { cartItems } = props; //check if const it is needed
//     let a = "item";
//     return (
//         <div>
//             {
//                 cartItems.length === 0 ?
//                     (<div className="cart cart-header">Cart is empty</div>)
//                     :
//                     (<div className="cart cart-header">You have {cartItems.length} {cartItems.length > 1 ? a += 's' : a} in the cart {""}</div>)
//             }
//         </div>
//     )
// }
// export default Cart;
