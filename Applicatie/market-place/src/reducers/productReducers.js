import {
  FETCH_PRODUCTS,
  SORT_PRODUCTS,
  FETCH_CHUNK_OF_PRODUCTS,
} from "../types";

export const productsReducer = (state = {}, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS: {
      return { items: action.payload, filteredItems: action.payload };
    }
    case SORT_PRODUCTS: {
      return {
        ...state,
        sortValue: action.payload.sortValue,
        filteredItems: action.payload.items,
      };
    }
    case FETCH_CHUNK_OF_PRODUCTS: {
      return {
        ...state,
        chunkSize: action.payload.chunkSize,
        filteredItems: action.payload.items,
      };
    }
    default: {
      return state;
    }
  }
};
